﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.MovieActors {
	public class MovieActorData {

		[Required(ErrorMessage = "Character is required")]
		[MaxLength(100, ErrorMessage = "The character name can't be greater than {1}")]
		public string Character { get; set; }

		[Required(ErrorMessage = "Movie is required")]
		public Guid MovieId { get; set; }

		[Required(ErrorMessage = "Actor is required")]
		public Guid ActorId { get; set; }
	}
}
