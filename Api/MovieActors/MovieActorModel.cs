﻿using Api.Actors;
using Api.Movies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.MovieActors {
	public class MovieActorModel {
		public Guid Id { get; set; }
		public string Character { get; set; }
		public ActorModel Actor { get; set; }
		public MovieModel Movie { get; set; }
	}
}
