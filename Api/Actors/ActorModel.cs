﻿using Api.MovieActors;
using System;
using System.Collections.Generic;
using System.Text;

namespace Api.Actors {
	public class ActorModel {
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Birthplace { get; set; }
	}
}
