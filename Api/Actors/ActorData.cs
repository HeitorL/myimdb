﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Api.Actors {
	public class ActorData {
		
		[Required(ErrorMessage = "Name is required")]
		[MaxLength(255, ErrorMessage = "The name can't be greater than {1}")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Birthplace is required")]
		[MaxLength(255, ErrorMessage = "The birthplace can't be greater than {1}")]
		public string Birthplace { get; set; }
		
	}
}
