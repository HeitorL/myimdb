using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyImdb.Business;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Configuration;
using MyImdb.Entities;
using MyImdb.Services;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;

namespace MyImdb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddControllersWithViews(o => {
				o.Filters.Add<HandleExceptionFilter>();
				o.Filters.Add<ValidateModelStateAttribute>();
			})
			.AddNewtonsoftJson()
			.AddRazorRuntimeCompilation();

			

            // Database connection
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

			// Registering Repositories
			services.AddScoped<MovieRepository>();
			services.AddScoped<GenreRepository>();
			services.AddScoped<ActorRepository>();
			services.AddScoped<MovieActorRepository>();
			// Registering Services
			services.AddScoped<MovieService>();
			services.AddScoped<GenreService>();
			services.AddScoped<ActorService>();
			services.AddScoped<MovieActorService>();

            services.AddScoped<ExceptionBuilder>();
			
			services.AddSingleton<ModelConverter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
				endpoints.MapControllers();
            });
        }
    }
}
