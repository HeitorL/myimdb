﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyImdb.Models;
using MyImdb.ViewModels;
using MyImdb.Business.Services;
using MyImdb.Business.Repositories;
using MyImdb.Services;
using Api.Movies;
using MyImdb.Entities;

namespace MyImdb.Controllers
{
	[Route("api/movies")]
	[ApiController]
	public class MovieController : Controller
    {
		private readonly MovieService movieService;
		private readonly MovieRepository movieRepository;
		private readonly ModelConverter mc;
		public MovieController(MovieService movieService, MovieRepository movieRepository, ModelConverter mc) {
			this.movieService = movieService;
			this.movieRepository = movieRepository;
			this.mc = mc;
		}

		[HttpGet]
		public async Task<List<MovieModel>> List(int n = 20) {
			var movies = await movieRepository.SelectTopNAsync(n);

			return movies.ConvertAll(m => mc.ToModel(m));
		}

		[HttpGet("{id}")]
		public async Task<MovieModel> Get(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);

			return mc.ToModel(movie);
		}


		[HttpPost]
		public async Task<MovieModel> Create(MovieData request) {
			var movie = await movieService.CreateAsync(request.Rank, request.Title, request.Year, request.Storyline, request.GenreId);

			return mc.ToModel(movie);
		}

		[HttpPut("{id}")]
		public async Task<MovieModel> Update(Guid id, MovieData request) {
			var movie = await movieRepository.SelectByIdAsync(id);

			await movieService.UpdateAsync(movie, request.Title, request.Rank, request.Storyline, request.Year, request.GenreId);

			return mc.ToModel(movie);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movie = await movieRepository.SelectByIdAsync(id);
			await movieService.DeleteAsync(movie);
		}

	}
}
