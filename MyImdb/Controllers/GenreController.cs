﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyImdb.Services;
using Api.Genres;
using MyImdb.Business.Services;

namespace MyImdb.Controllers {
	[Route("api/genres")]
	[ApiController]
	public class GenreController : Controller {
		
		private readonly GenreRepository genreRepository;
		private readonly ModelConverter mc;
		private readonly GenreService genreService;

		public GenreController(GenreRepository genreRepository, ModelConverter mc, GenreService genreService) {
			this.genreRepository = genreRepository;
			this.mc = mc;
			this.genreService = genreService;
		}

		[HttpGet]
		public async Task<List<GenreModel>> List(int n = 20) {
			var genres = await genreRepository.SelectTopNAsync(n);

			return genres.ConvertAll(g => mc.ToModel(g));
		}

		[HttpGet("{id}")]
		public async Task<GenreModel> Get(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);
			return mc.ToModel(genre);
		}

		[HttpPost]
		public async Task<GenreModel> Create(GenreData request) {
			var genre = await genreService.CreateAsync(request.Name);

			return mc.ToModel(genre);
		}

		[HttpPut("{id}")]
		public async Task<GenreModel> Update(Guid id, GenreData request) {
			var genre = await genreRepository.SelectByIdAsync(id);

			await genreService.UpdateAsync(genre, request.Name);

			return mc.ToModel(genre);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var genre = await genreRepository.SelectByIdAsync(id);

			await genreService.DeleteAsync(genre);
		}
	}
}
