﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyImdb.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MyImdb.ViewModels;

namespace MyImdb.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> logger;

        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger;
        }

        /*public IActionResult Index()
        {
            var topMovies = Movie.SelectTopMovies().ConvertAll(m => new TopMovieListViewModel { Title = m.Title });
            return View(topMovies);
        }*/

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
