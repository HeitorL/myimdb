﻿using Api.MovieActors;
using Api.Movies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyImdb.Business.Repositories;
using MyImdb.Business.Services;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Controllers {
	[Route("api/movieactors")]
	[ApiController]
	public class MovieActorController : Controller {
		private readonly MovieActorRepository movieActorRepository;
		private readonly MovieActorService movieActorService;
		private readonly ModelConverter mc;

		public MovieActorController(
			MovieActorRepository movieActorRepository,
			MovieActorService movieActorService,
			ModelConverter mc
			) {
			this.movieActorRepository = movieActorRepository;
			this.movieActorService = movieActorService;
			this.mc = mc;
		}

		[HttpPost]
		public async Task<MovieActorModel> Create(MovieActorData request) {
			var movieActor = await movieActorService.CreateAsync(request.Character, request.MovieId, request.ActorId);
			return mc.ToModel(movieActor);
		}

		[HttpGet("movie/{id}")]
		public async Task<List<MovieActorModel>> ListActors(Guid id) {
			var movieActors = await movieActorRepository.SelectByMovieIdAsync(id);

			return movieActors.ConvertAll(ma => mc.ToModel(ma));
		}

		[HttpGet("actor/{id}")]
		public async Task<List<MovieActorModel>> ListMovies(Guid id) {
			var movieActors = await movieActorRepository.SelectByActorIdAsync(id);

			return movieActors.ConvertAll(ma => mc.ToModel(ma));
		}

		[HttpPut("{id}")]
		public async Task<MovieActorModel> Update(Guid id, MovieActorData request) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);
			await movieActorService.UpdateAsync(movieActor, request.Character, request.ActorId, request.MovieId);

			return mc.ToModel(movieActor);
		}

		[HttpDelete("{id}")]
		public async Task Delete(Guid id) {
			var movieActor = await movieActorRepository.SelectByIdAsync(id);
			await movieActorService.DeleteAsync(movieActor);
		}

	}
}
