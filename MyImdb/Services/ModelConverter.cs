﻿using Api.Actors;
using Api.Genres;
using Api.MovieActors;
using Api.Movies;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Services {
	public class ModelConverter {
		public ModelConverter() { }

		public GenreModel ToModel(Genre genre) {
			return new GenreModel() {
				Id = genre.Id,
				Name = genre.Name
			};
		}

		public MovieModel ToModel(Movie movie) {
			return new MovieModel() {
				Id = movie.Id,
				Title = movie.Title,
				Rank = movie.Rank,
				Year = movie.Year,
				Storyline = movie.Storyline,
				Genre = ToModel(movie.Genre)
			};
		}

		public ActorModel ToModel(Actor actor) {
			return new ActorModel() {
				Id = actor.Id,
				Name = actor.Name,
				Birthplace = actor.Birthplace
			};
		}

		public MovieActorModel ToModel(MovieActor movieActor) {
			return new MovieActorModel() {
				Id = movieActor.Id,
				Character = movieActor.Character,
				Actor = ToModel(movieActor.Actor),
				Movie = ToModel(movieActor.Movie)
			};
		}
	}
}
