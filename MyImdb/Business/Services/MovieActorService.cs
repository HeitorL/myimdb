﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;
using MyImdb.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb.Business.Services {
	public class MovieActorService {
		private readonly AppDbContext dbContext;
		private readonly MovieRepository movieRepository;
		private readonly ActorRepository actorRepository;
		private readonly MovieActorRepository movieActorRepository;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieActorService(
			AppDbContext dbContext,
			MovieRepository movieRepository,
			ActorRepository actorRepository,
			MovieActorRepository movieActorRepository,
			ExceptionBuilder exceptionBuilder
		) {
			this.dbContext = dbContext;
			this.movieRepository = movieRepository;
			this.actorRepository = actorRepository;
			this.movieActorRepository = movieActorRepository;
			this.exceptionBuilder = exceptionBuilder;
		}


		public async Task<MovieActor> CreateAsync(string character, Guid movieId, Guid actorId) {
			var movie = await movieRepository.SelectByIdAsync(movieId);
			var actor = await actorRepository.SelectByIdAsync(actorId);

			var movieActor = await movieActorRepository.CreateAsync(actorId, movieId, character);

			await dbContext.SaveChangesAsync();

			return movieActor;
		}

		public async Task UpdateAsync(MovieActor movieActor, string character, Guid actorId, Guid movieId) {
			movieActor.Character = character;

			var actor = await actorRepository.SelectByIdAsync(actorId);
			movieActor.ActorId = actorId;
			var movie = await movieRepository.SelectByIdAsync(movieId);
			movieActor.MovieId = movieId;

			await dbContext.SaveChangesAsync();

		}

		public async Task DeleteAsync(MovieActor movieActor) {
			dbContext.Remove(movieActor);
			await dbContext.SaveChangesAsync();
		}
	}
}
