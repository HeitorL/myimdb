﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Business.Repositories;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Enums;

namespace MyImdb.Business.Services {
	public class MovieService {
		private readonly MovieRepository movieRepository;
		private readonly ExceptionBuilder exceptionBuilder;
		private readonly AppDbContext dbContext;
		private readonly GenreRepository genreRepository;

		public MovieService(
			MovieRepository movieRepository,
			ExceptionBuilder exceptionBuilder,
			AppDbContext dbContext,
			GenreRepository genreRepository
		) {
			this.movieRepository = movieRepository;
			this.exceptionBuilder = exceptionBuilder;
			this.dbContext = dbContext;
			this.genreRepository = genreRepository;
		}

		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, Guid genreId) {
			var existingMovie = await movieRepository.SelectByTitleAsync(title);
			
			if(existingMovie != null) {
				throw exceptionBuilder.Api(ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			var genre = await genreRepository.SelectByIdAsync(genreId);
			
			var movie = await movieRepository.CreateAsync(rank, title, year, storyline, genreId);
			await dbContext.SaveChangesAsync();

			// Pq n fazer isso?
			//movie.Genre = genre;

			return movie;
		}

		public async Task UpdateAsync(Movie movie, string title, int rank, string storyline, int year, Guid genreId) {
			if(await dbContext.Movies.AnyAsync(m => m.Id != movie.Id && m.Title == title)) {
				throw exceptionBuilder.Api(ErrorCodes.MovieTitleAlreadyExists, new { title });
			}

			movie.Title = title;
			movie.Rank = rank;
			movie.Storyline = storyline;
			movie.Year = year;

			var genre = await genreRepository.SelectByIdAsync(genreId);

			movie.GenreId = genreId;
			
			// Pq n fazer isso?
			//movie.Genre = genre;
			
			await dbContext.SaveChangesAsync();

		}

		public async Task DeleteAsync(Movie movie) {
			dbContext.Remove(movie);
			await dbContext.SaveChangesAsync();
		}
	}
}
