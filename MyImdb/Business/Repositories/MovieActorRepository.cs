﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Enums;

namespace MyImdb.Business.Repositories {
	public class MovieActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieActorRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<MovieActor> CreateAsync(Guid actorId, Guid movieId, string character) {
			var movieActor = new MovieActor() {
				Id = Guid.NewGuid(),
				ActorId = actorId,
				MovieId = movieId,
				Character = character
			};

			await dbContext.AddAsync(movieActor);
			return movieActor;
		}

		public async Task<List<MovieActor>> SelectByMovieIdAsync(Guid movieId) {
			return await dbContext.MovieActors
							.Include(ma => ma.Actor)
							.Include(ma => ma.Movie)
							.Include(ma => ma.Movie.Genre)
							.Where(ma => ma.MovieId == movieId)
							.ToListAsync();
		}

		public async Task<List<MovieActor>> SelectByActorIdAsync(Guid actorId) {
			return await dbContext.MovieActors
							.Include(ma => ma.Actor)
							.Include(ma => ma.Movie)
							.Include(ma => ma.Movie.Genre)
							.Where(ma => ma.ActorId == actorId)
							.ToListAsync();
		}

		public async Task<MovieActor> SelectByIdAsync(Guid id) {
			var movieActor = await dbContext.MovieActors
					.FirstOrDefaultAsync(ma => ma.Id == id) ??
				throw exceptionBuilder.Api(ErrorCodes.MovieActorNotFound, new { id });
			return movieActor;
		}
	}
}
