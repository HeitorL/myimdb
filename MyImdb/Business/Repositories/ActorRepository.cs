﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Enums;

namespace MyImdb.Business.Repositories {
	public class ActorRepository {
		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public ActorRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<List<Actor>> SelectTopNAsync(int n = 20) {
			return await dbContext.Actors.OrderBy(a => a.Name).AsQueryable().Take(n).ToListAsync(); 
		}

		public async Task<Actor> SelectByIdAsync(Guid id) {
			var actor = await dbContext.Actors.FirstOrDefaultAsync(a => a.Id == id) ??
				throw exceptionBuilder.Api(ErrorCodes.ActorNotFound, new { id });
			return actor;
		}

		public async Task<Actor> CreateAsync(string name, string birthplace) {
			var actor = new Actor() {
				Id = new Guid(),
				Name = name,
				Birthplace = birthplace
			};

			await dbContext.AddAsync(actor);
			return actor;
		}
	}
}
