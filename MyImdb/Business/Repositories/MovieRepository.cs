﻿using Microsoft.EntityFrameworkCore;
using MyImdb.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Api.Enums;

namespace MyImdb.Business.Repositories {
	public class MovieRepository {

		private readonly AppDbContext dbContext;
		private readonly ExceptionBuilder exceptionBuilder;

		public MovieRepository(AppDbContext dbContext, ExceptionBuilder exceptionBuilder) {
			this.dbContext = dbContext;
			this.exceptionBuilder = exceptionBuilder;
		}

		public async Task<Movie> CreateAsync(int rank, string title, int year, string storyline, Guid genreId) {
			var movie = new Movie() {
				Id = Guid.NewGuid(),
				Rank = rank,
				Title = title,
				Year = year,
				Storyline = storyline,
				CreationDate = DateTimeOffset.Now,
				GenreId = genreId,
			};
			await dbContext.AddAsync(movie);
			return movie;
		}

		public async Task<Movie> SelectByIdAsync(Guid id) {
			var movie = await dbContext.Movies
					.Include(m => m.Genre)
					.FirstOrDefaultAsync(m => m.Id == id) ??
				throw exceptionBuilder.Api(ErrorCodes.MovieNotFound, new { id });
			return movie;
		}

		public async Task<List<Movie>> SelectTopNAsync(int n = 20) {
			var query = dbContext.Movies.Include(m => m.Genre).OrderBy(m => m.Title).AsQueryable();
			query = query.Take(n);

			return await query.ToListAsync(); // o acesso ao BD ocorre aqui
        }

		public async Task<Movie> SelectByTitleAsync(string title) {
			return await dbContext.Movies.Include(m => m.Genre).FirstOrDefaultAsync(m => m.Title == title);
		}

		public async Task<List<Movie>> SelectByGenreIdAsync(Guid genreId) {
			return await dbContext.Movies.Include(m => m.Genre).Where(m => m.GenreId == genreId).ToListAsync();
		}

	}
}
