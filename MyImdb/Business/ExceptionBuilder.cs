﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api;
using static Api.Enums;

namespace MyImdb.Business {
	public class ExceptionBuilder {
		public ApiException Api(ErrorCodes code, object details = null) {
			var message = string.Empty;
			switch (code) {
				case ErrorCodes.Unknown:
					message = "An Unknown error has ocurred";
					break;
				case ErrorCodes.MovieTitleAlreadyExists:
					message = "A movie with the provided title already exists";
					break;
				case ErrorCodes.GenreNotFound:
					message = "Genre not found";
					break;
				case ErrorCodes.GenreNameAlreadyExists:
					message = "A genre with the provided name already exists";
					break;
				case ErrorCodes.MovieNotFound:
					message = "Movie not found";
					break;
				case ErrorCodes.ActorNotFound:
					message = "Actor not found";
					break;
				case ErrorCodes.MovieActorNotFound:
					message = "MovieActor not found";
					break;
				default:
					break;
			}

			return new ApiException(new ErrorModel() { Code = code, Message = message, Details = getDetailsDictionary(details) });
		}

		private static Dictionary<string, string> getDetailsDictionary(object details) {
			var dic = new Dictionary<string, string>();
			foreach(var descriptor in details.GetType().GetProperties()) {
				dic[descriptor.Name] = descriptor.GetValue(details, null)?.ToString() ?? "null";
			}
			return dic;
		}
	}
}
